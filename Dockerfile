FROM debian:stretch

ADD scripts/build.sh /build.sh
ADD scripts/run.sh /run.sh

RUN chmod +x /run.sh /build.sh && sync && sleep 1 && /build.sh

RUN touch /etc/netdata/.opt-out-from-anonymous-statistics

WORKDIR /

ENV NETDATA_PORT=19999 SMTP_TLS=on SMTP_STARTTLS=on SMTP_SERVER=smtp.example.com SMTP_PORT=587 SMTP_FROM=localhost

# supposedly it now respects this
ENV DO_NOT_TRACK=1

EXPOSE $NETDATA_PORT

VOLUME /etc/netdata/override

ENTRYPOINT ["/run.sh"]
