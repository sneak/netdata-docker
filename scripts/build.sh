#!/bin/bash
set -e
DEBIAN_FRONTEND=noninteractive

# belt and suspenders
export DO_NOT_TRACK=1

# install dependencies for build
apt-get -qq update
apt-get -y install zlib1g-dev uuid-dev libmnl-dev gcc make curl git autoconf autogen automake pkg-config netcat-openbsd jq libuv1-dev liblz4-dev libjudy-dev libssl-dev
apt-get -y install autoconf-archive lm-sensors nodejs python python-mysqldb python-yaml libjudydebian1 libuv1 liblz4-1 openssl
apt-get -y install msmtp msmtp-mta apcupsd fping

# fetch netdata

git clone https://github.com/netdata/netdata.git /netdata.git
cd /netdata.git
# v1.19.0 release:
git checkout 5000257f0171271cb3ee2cf0fe02e8a2154ddf2e

# patch out spyware hostnames:
find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#google-analytics.com#nospyware.example.com#gI'
find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#googletagmanager.com#nospyware.example.com#gI'
find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#my-netdata.io#nospyware.example.com#gI'
#find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#githubusercontent.com#nospyware.example.com#gI'
#find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#github.com#nospyware.example.com#gI'
find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#netdata.cloud#nospyware.example.com#gI'
find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#pagerduty.com#nospyware.example.com#gI'
find . -type f -print0 | xargs -0 -P 1 -- sed -i 's#shields.io#nospyware.example.com#gI'

# use the provided installer
./netdata-installer.sh --dont-wait --dont-start-it --disable-telemetry

touch /etc/netdata/.opt-out-from-anonymous-statistics


# remove build dependencies
cd /
rm -rf /netdata.git

dpkg -P zlib1g-dev uuid-dev libmnl-dev gcc make git autoconf autogen automake pkg-config libuv1-dev liblz4-dev libjudy-dev libssl-dev
apt-get -y autoremove
apt-get clean
rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# symlink access log and error log to stdout/stderr
ln -sf /dev/stdout /var/log/netdata/access.log
ln -sf /dev/stdout /var/log/netdata/debug.log
ln -sf /dev/stderr /var/log/netdata/error.log
